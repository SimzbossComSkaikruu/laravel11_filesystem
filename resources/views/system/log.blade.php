@php use Carbon\Carbon; @endphp
<x-app-layout>
    <section class="container mx-auto p-6 font-mono">
        <div class="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
            <div class="w-full overflow-x-auto">
                <table class="w-full">
                    <thead>
                    <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                        <th class="px-4 py-3">{{ __('User') }}</th>
                        <th class="px-4 py-3">{{ __('Current Role') }}</th>
                        <th class="px-4 py-3">{{ __('Action') }}</th>
                        <th class="px-4 py-3">{{ __('Description') }}</th>
                        <th class="px-4 py-3">{{ __('TimeStamp') }}</th>
                    </tr>
                    </thead>
                    <tbody class="bg-white">
                    @forelse ($logs as $file)
                        <tr class="text-gray-700">
                            <td class="px-4 py-3 border">
                                <div class="flex items-center text-sm">
                                    <div>
                                        <p class="font-semibold text-black">{{ $file->user->name }}</p>
                                        {{--                                        <p class="text-xs text-gray-600">file</p>--}}
                                    </div>
                                </div>
                            </td>
                            <td class="px-4 py-3 text-ms font-semibold border">@foreach($file->user->roles()->pluck('name') as $role)
                                    {!! $role !!}
                                @endforeach</td>
                            <td class="px-4 py-3 text-xs border">
                                <span
                                    class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm"> {{ $file->action }} </span>
                            </td>
                            <td class="px-4 py-3 text-xs border">
                                <span
                                    class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm"> {{ $file->notes }} </span>
                            </td>
                            <td class="px-4 py-3 text-sm border">{{Carbon::parse($file->created_at )->toFormattedDateString()}} - {{ Carbon::parse($file->created_at )->diffForHumans(short: true)}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td class="px-4 py-3 text-sm border" colspan="6">No files found</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <div class="p-2">
                    {{ $logs->links() }}
                </div>
            </div>
        </div>
    </section>
</x-app-layout>
