@php use Carbon\Carbon; @endphp
<x-app-layout>
    <x-slot name="header">
        <div class="columns-3">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Files') }}
            </h2>
            <div class="column-2">
                <form action="{{route('search')}}" method="POST">
                    @csrf
                    <div class="float-right">

                        <input type="text"
                               {{--                           wire:model="search"--}}
                               name="search"
                               class="h-14 w-96 pr-8 pl-5 rounded z-0 focus:shadow focus:outline-none"
                               placeholder="Type To Search Files.">
                        <div class="absolute top-4 right-3">
                            <i class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i>
                        </div>


                        {{--                <h2 x-text="$wire.get('search')"></h2>--}}
                    </div>
                    <button type="submit"
                            class="h-14 w-14 text-gray-400 z-20 hover:text-gray-500">
                        <i class="fa fa-times"></i>
                        search
                    </button>
                </form>
            </div>
{{--            <a href="{{ route('files.import') }}"--}}
{{--               class="float-right bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">--}}
{{--                Import--}}
{{--            </a>--}}
        </div>
    </x-slot>


    @session('success')
    <x-alert type="success" :message="Session::get('success')"/>
    @endsession
    @session('error')
    <x-alert type="error" :message="Session::get('error')"/>
    @endsession
    <section class="container mx-auto p-6 font-mono">
        <div class="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
            <div class="w-full overflow-x-auto">
                <table class="w-full">
                    <thead>
                    <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                        <th class="px-4 py-3">{{ __('Name') }}</th>
                        <th class="px-4 py-3">{{ __('Size') }}</th>
                        <th class="px-4 py-3">{{ __('Type') }}</th>
                        <th class="px-4 py-3">{{ __('Uploaded At') }}</th>
                        <th class="px-4 py-3">{{ __('Uploaded By') }}</th>
                        <th class="px-4 py-3">{{ __('Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody class="bg-white">
                    @forelse ($files as $file)
                        <tr wire:key="{{$file->id}}" class="text-gray-700">
                            <td class="px-4 py-3 border">
                                <div class="flex items-center text-sm">
                                    <div>
                                        <p class="font-semibold text-black">{{ $file->name }}</p>
                                        <p class="text-xs text-gray-600">file</p>
                                    </div>
                                </div>
                            </td>
                            <td class="px-4 py-3 text-ms font-semibold border">{{ $file->size }}</td>
                            <td class="px-4 py-3 text-xs border">
                                <span
                                    class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm"> {{ $file->extension }} </span>
                            </td>
                            <td class="px-4 py-3 text-sm border">{{ Carbon::parse($file->created_at )->diffForHumans(short: true)}}</td>
                            <td class="px-4 py-3 text-sm border">{{ $file->user->name }}</td>
                            <td class="px-4 py-3 text-sm border">
                                <a href="{{route('files.download',$file->id)}}"
                                   class="text-blue-600 hover:text-blue-900">Download</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="px-4 py-3 text-sm border" colspan="6">No files found</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <div class="p-2">
                    {{ $files->links() }}
                </div>
            </div>
        </div>
    </section>
</x-app-layout>

