@php use Illuminate\Support\Facades\DB; @endphp
<x-guest-layout>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Create User</h2>
        <!-- Name -->
        <div>
            <x-input-label for="name" :value="__('Name')"/>
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required
                          autofocus autocomplete="name"/>
            <x-input-error :messages="$errors->get('name')" class="mt-2"/>
        </div>

        <!-- Email Address -->
        <div class="mt-4">
            <x-input-label for="email" :value="__('Email')"/>
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required
                          autocomplete="username"/>
            <x-input-error :messages="$errors->get('email')" class="mt-2"/>
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-input-label for="password" :value="__('Password')"/>

            <x-text-input id="password" class="block mt-1 w-full"
                          type="password"
                          name="password"
                          required autocomplete="new-password"/>

            <x-input-error :messages="$errors->get('password')" class="mt-2"/>
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-input-label for="password_confirmation" :value="__('Confirm Password')"/>

            <x-text-input id="password_confirmation" class="block mt-1 w-full"
                          type="password"
                          name="password_confirmation" required autocomplete="new-password"/>

            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>
        </div>

        <fieldset class="mt-2">
            <legend class="">User Roles</legend>
            <div class="space-y-5">
                @foreach(DB::table('user_roles')->where('status','active')->get() as $role)
                    <div class="relative flex items-start">
                        <div class="flex h-6 items-center">
                            <input id="comments" aria-describedby="comments-description" name="roles[]"
                                   type="checkbox"
                                   value="{{$role->name}}"
                                   class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                        </div>
                        <div class="ml-3 text-sm leading-6">
                            <label for="comments" class="font-medium text-gray-900">{{$role->name}}</label>
                            @if($role->name != 'super')
                                <span id="comments-description" class="text-gray-500"><span class="sr-only"></span>user always {{ $role->name }} a file.</span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </fieldset>

        <div class="flex items-center justify-end mt-4">
            {{--            <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('login') }}">--}}
            {{--                {{ __('Already registered?') }}--}}
            {{--            </a>--}}

            <x-primary-button class="ms-4">
                {{ __('Register') }}
            </x-primary-button>
        </div>
    </form>
</x-guest-layout>
