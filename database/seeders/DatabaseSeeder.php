<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        $roles = ['create', 'read', 'update', 'delete', 'super'];

        if (!DB::table('user_roles')->count() > 0) {
            foreach ($roles as $role) {
                DB::table('user_roles')->insert([
                    'name' => $role,
                    'description' => $role,
                    'status' => 'active',
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }


        if (User::all()->count() > 0) {
            return;
        }

        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@hararecity.co.zw',
            'password' => Hash::make('12345678'),
        ]);

        $user->assignUserRole('super');

    }
}
