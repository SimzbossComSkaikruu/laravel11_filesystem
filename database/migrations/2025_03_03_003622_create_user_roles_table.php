<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $roles = ['create', 'read', 'update', 'delete', 'super'];

        if (!Schema::hasTable('user_roles'))
            Schema::create('user_roles', function (Blueprint $table) use ($roles) {
                $table->id();
                $table->enum('name', $roles);
                $table->string('description')->nullable();
                $table->string('status')->default('active');
                $table->timestamps();
            });

        if (!Schema::hasTable('user_role_pivot')) {
            Schema::create('user_role_pivot', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
            });
        }

        if (DB::table('user_roles')->count() > 0) {
            return;
        }

        foreach ($roles as $role) {
            DB::table('user_roles')->insert([
                'name' => $role,
                'description' => $role,
                'status' => 'active',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_roles');
    }
};
