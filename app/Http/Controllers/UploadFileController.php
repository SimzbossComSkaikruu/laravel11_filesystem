<?php

namespace App\Http\Controllers;

use App\Models\Hfile;
use App\Models\SystemLog;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadFileController extends Controller
{
    public function dashboard(): Factory|\Illuminate\Foundation\Application|View|Application
    {
        SystemLog::create([
            "action" => "read",
            'user_id' => Auth::user()->id,
            'notes' => 'Opened Dashboard',
        ]);

        return view('dashboard', ['files' => Hfile::paginate(15)]);
    }

    public function clearSearch(Request $request): RedirectResponse
    {
        $request->validate([
            'search' => 'required|string'
        ]);

        SystemLog::create([
            "action" => "read",
            'user_id' => Auth::user()->id,
            'notes' => 'Search The Files In The System Which Contains "' . $request->search . '"',
        ]);

        return redirect()->route('get-search', ['search' => $request->input('search')]);
    }

    public function search(Request $request): Factory|\Illuminate\Foundation\Application|View|Application
    {
//        dd($request->all());
        $request->validate([
            'search' => 'required|string'
        ]);

        $search = Hfile::where('name', 'like', '%' . $request->input('search') . '%')
            ->orWhere('type', 'like', '%' . $request->input('search') . '%')
            ->orWhere('size', 'like', '%' . $request->input('search') . '%')
            ->orWhere('extension', 'like', '%' . $request->input('search') . '%')
            ->orWhere('mime_type', 'like', '%' . $request->input('search') . '%')
            ->orWhere('disk', 'like', '%' . $request->input('search') . '%')
            ->orWhere('visibility', 'like', '%' . $request->input('search') . '%')
            ->orWhere('created_at', 'like', '%' . $request->input('search') . '%')
            ->paginate();

        $string = $request->input('search');

        SystemLog::create([
            "action" => "read",
            'user_id' => Auth::user()->id,
            'notes' => 'Opened Search Dashboard : Files With Search String "' . $string . '"',
        ]);

        return view('dashboard', ['files' => $search, 'string' => $string]);
    }

    public function store(Request $request): RedirectResponse
    {
        try {
            $request->validate([
                "_token" => "required|string",
                "file_name" => "required|string",
                'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx,txt,csv|max:2048'
            ]);

            $file = $request->file('file');
            $path = $file->storeAs('public/files', time() . $file->hashName());

            $newFile = new Hfile([
                'name' => $request->input("file_name", time() . "Untitled"),
                'path' => $path,
                'type' => $file->getClientMimeType(),
                'size' => $file->getSize(),
                'extension' => $file->getClientOriginalExtension(),
                'mime_type' => $file->getMimeType(),
                'disk' => 'local',
                'visibility' => 'public',
                'user_id' => auth()->id()
            ]);
            $newFile->save();

            SystemLog::create([
                "action" => "write",
                'user_id' => Auth::user()->id,
                'notes' => 'Created a new file : File Name Created "' . $newFile->name . '"',
            ]);

            return redirect()->route('dashboard')->with('success', 'File uploaded successfully');

        } catch (Exception $e) {
            dd($e);
            return redirect()->route('dashboard')->with('error', 'Error uploading file');
        }
    }
}
