<?php

namespace App\Livewire;

use App\Models\Hfile;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class UploadFile extends Component
{
    use WithPagination;

    public string $search = '';
    public  $searched = [];


    public function render()
    {
        $files = Hfile::orderBy('created_at', 'desc')
            ->paginate(10);

        return view('livewire.upload-file', compact('files'));
    }
}
