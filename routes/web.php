<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UploadFileController;
use App\Livewire\UploadFile;
use App\Models\Hfile;
use App\Models\SystemLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [AuthenticatedSessionController::class, 'create']);

Route::get('/dashboard', UploadFile::class)->middleware(['auth', 'verified'])->name('dashboard');

Route::post("/search", [UploadFileController::class, 'clearSearch'])->middleware(['auth', 'verified'])->name('search');
Route::get("/get-search", [UploadFileController::class, 'search'])->middleware(['auth', 'verified'])->name('get-search');

Route::get('/upload-file', function () {
    SystemLog::create([
        "action" => "read",
        'user_id' => Auth::user()->id,
        'notes' => 'Open Create Upload File Dashboard',
    ]);

    return view('upload-file');
})->middleware(['auth', 'verified'])->name('files.import');


Route::get('system/log', function () {
    $logs = SystemLog::paginate(10);

    return view('system.log', ['logs' => $logs]);
})->middleware(['auth', 'verified'])->name('system.log');

Route::post('/upload-file', [UploadFileController::class, 'store'])->middleware(['auth', 'verified'])->name('files.store');
Route::get('/download-file/{file}', function (Hfile $file) {

    SystemLog::create([
        "action" => "Update",
        'user_id' => Auth::user()->id,
        'notes' => 'Download File For Read And Update : File Name "' . $file->name . '"',
    ]);

    return response()
        ->download(storage_path("app/{$file->path}"), $file->name . '.' . $file->extension, ['Content-Type' => $file->mime_type]);
})
    ->middleware(['auth', 'verified'])
    ->name('files.download');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
